# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* use pure JavaScript to build **Auto Complete Widget** without server-side
* 1.0

### How do I get set up? ###

* new an empty html
* include style.css & autocomplete.js
* add some element in previous html

   ps: you can change the id of input and div if you need


```
#!html
    <input id="myinput" />
```

* add some script to create auto-complete widget

```
#!javascript
    var input = document.getElementById("myinput");
    var autocomplete = new Autocomplete(input, {
        minChars: 1,
        maxItems: 30,
        filter: Autocomplete.FILTER_STARTSWITH,
        replace: function () {
            // clear input after selection
            this.input.value = '';
        }
    });
```
* put data to the autocomplete, you can use static array or load data by file/server
   
```
#!javascript

   var loadJSON = function (fileUrl, callback) {
        var xobj = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xobj.overrideMimeType('application/json');
        xobj.open('GET', fileUrl, true);
        xobj.onreadystatechange = function () {
            // .open will NOT return a value but simply returns undefined in async mode so use a callback
            if (xobj.readyState == 4 && xobj.status == '200')
                callback(xobj.responseText);
        }
        xobj.send(null);
    };
    loadJSON('data/timezone.json', function (data) {
        // put data
        autocomplete.list = JSON.parse(data).zoneSet;
    });
```


### How to run tests? ###
* input the location name
![autocomplete.PNG](https://bitbucket.org/repo/qyA5Ay/images/318366851-autocomplete.PNG)