﻿(function () {

    var _ = function (input, o) {
        var theWidget = this;

        // Initial the Auto-Complete Widget
        this.input = $(input);
        this.input.setAttribute("autocomplete", "off");
        this.input.setAttribute("aria-autocomplete", "list");

        o = o || {};

        // call function 'configure' with the given 'this' and other arguments
        configure.call(this, {
            minChars: 2,
            maxItems: 10,
            autoFirst: false,
            filter: _.FILTER_CONTAINS,
            sort: _.SORT_BYLENGTH,
            item: function (text, input) {
                return $.create("li", {
                    innerHTML: text.replace(RegExp($.regExpEscape(input.trim()), "gi"), "$&"),
                    "aria-selected": "false"
                });
            },
            replace: function (text) {
                this.input.value = text;
            }
        }, o);

        this.index = -1;

        // Create necessary elements
        this.container = $.create("div", {
            className: "autocomplete",
            around: input
        });

        this.container.li = $.create("li", {
            className: "tag-new",
            around: this.container
        });

        this.container.ul = $.create("ul", {
            className: "tag",
            around: this.container.li
        });

        this.ul = $.create("ul", {
            hidden: "",
            inside: this.container
        });

        this.status = $.create("span", {
            className: "visually-hidden",
            role: "status",
            "aria-live": "assertive",
            "aria-relevant": "additions",
            inside: this.container
        });

        // Bind events

        $.bind(this.input, {
            "input": this.evaluate.bind(this),
            "blur": this.close.bind(this),
            "keydown": function (evt) {
                var c = evt.keyCode;
                
                // If the dropdown `ul` is in view, then act on keydown for the following keys:
                // Enter / Esc / Up / Down
                if (theWidget.opened) {
                    if (c === 13 && theWidget.selected) { // Enter
                        evt.preventDefault();
                        theWidget.select();
                    }
                    else if (c === 27) { // Esc
                        theWidget.close();
                    }
                    else if (c === 38 || c === 40) { // Down/Up arrow
                        evt.preventDefault();
                        // 38: theWidget.previous(), 40: theWidget.next()
                        theWidget[c === 38 ? "previous" : "next"]();
                    }
                }
            },
            "mouseup": function (e) {
                if (document.activeElement === theWidget.input)
                    theWidget.evaluate();
            }
        });

        $.bind(this.input.form, { "submit": this.close.bind(this) });

        $.bind(this.ul, {
            "mousedown": function (evt) {
                var li = evt.target;

                if (li !== this) {

                    while (li && !/li/i.test(li.nodeName)) {
                        li = li.parentNode;
                    }

                    if (li) {
                        theWidget.select(li);
                    }
                }
            }
        });

        // handle the list(bcz list can be JS Property or Html Attribute)
        if (this.input.hasAttribute("list")) {
            this.list = "#" + input.getAttribute("list");
            input.removeAttribute("list");
        }
        else {
            this.list = this.input.getAttribute("data-list") || o.list || [];
        }

        //_.all.push(this);
    };

    _.prototype = {
        set list(list) {
            if (Array.isArray(list)) {
                this._list = list;
            }
            else if (typeof list === "string" && list.indexOf(",") > -1) {
                this._list = list.split(/\s*,\s*/);
            }
            else { // Element or CSS selector
                list = $(list);

                if (list && list.children) {
                    // HTMLCollection(list.children) is NOT an array, it should be converted to an Array
                    this._list = slice.apply(list.children).map(function (el) {
                        return el.innerHTML.trim();
                    });
                }
            }

            if (document.activeElement === this.input) {
                this.evaluate();
            }
        },

        get selected() {
            return this.index > -1;
        },

        get opened() {
            return this.ul && this.ul.getAttribute("hidden") == null;
        },

        close: function () {
            this.ul.setAttribute("hidden", "");
            this.index = -1;

            $.fire(this.input, "autocomplete-close");
        },

        open: function () {
            this.ul.removeAttribute("hidden");

            if (this.autoFirst && this.index === -1) {
                this.goto(0);
            }

            $.fire(this.input, "autocomplete-open");
        },

        next: function () {
            var count = this.ul.children.length;

            this.goto(this.index < count - 1 ? this.index + 1 : -1);
        },

        previous: function () {
            var count = this.ul.children.length;

            this.goto(this.selected ? this.index - 1 : count - 1);
        },

        // Should not be used, highlights specific item without any checks!
        goto: function (i) {
            var lis = this.ul.children;

            if (this.selected) {
                lis[this.index].setAttribute("aria-selected", "false");
            }

            this.index = i;

            if (i > -1 && lis.length > 0) {
                lis[i].setAttribute("aria-selected", "true");
                this.status.textContent = lis[i].textContent;
            }

            $.fire(this.input, "autocomplete-highlight");
        },

        select: function (selected) {
            selected = selected || this.ul.children[this.index];

            if (selected) {
                var prevented;

                $.fire(this.input, "autocomplete-select", {
                    text: selected.textContent,
                    preventDefault: function () {
                        prevented = true;
                    }
                });

                if (!prevented) {
                    this.replace(selected.textContent);
                    this.close();
                    $.fire(this.input, "autocomplete-selectcomplete");
                }

                // Insert Choice
                var el = $.createNewChoice({
                    tagName: 'li',
                    className: 'tag-choice',
                    children: [
                        {
                            tagName: 'span',
                            className: 'tag-label',
                            html: selected.textContent
                        },
                        {
                            tagName: 'a',
                            className: 'tag-close',
                            children: [
                                {
                                    tagName: 'span',
                                    className: 'text-icon',
                                    html: '×'
                                }
                            ]
                        }
                    ]
                });

                el.addEventListener("mousedown", function (e) {
                    this.remove();
                    e.stopPropagation();
                    e.preventDefault();
                });
                this.container.ul.insertBefore(el, this.container.li);
            }
        },

        evaluate: function () {
            var theWidget = this;
            var value = this.input.value;

            if (value.length >= this.minChars && this._list.length > 0) {
                this.index = -1;
                // Populate list with options that match
                this.ul.innerHTML = "";

                this._list
                    .filter(function (item) {
                        return theWidget.filter(item, value);
                    })
                    .sort(this.sort)
                    .every(function (text, i) {
                        theWidget.ul.appendChild(theWidget.item(text, value));

                        return i < theWidget.maxItems - 1;
                    });

                if (this.ul.children.length === 0) {
                    this.close();
                } else {
                    this.open();
                }
            }
            else {
                this.close();
            }
        }
    };

    // Static methods/properties

    //_.all = [];

    _.FILTER_CONTAINS = function (text, input) {
        return RegExp($.regExpEscape(input.trim()), "i").test(text);
    };

    _.FILTER_STARTSWITH = function (text, input) {
        return RegExp("^" + $.regExpEscape(input.trim()), "i").test(text);
    };

    _.SORT_BYLENGTH = function (a, b) {
        if (a.length !== b.length) {
            return a.length - b.length;
        }

        return a < b ? -1 : 1;
    };

    // Private functions

    // Get the final configuration with default value & user setting 
    function configure(properties, o) {
        for (var i in properties) {
            var initial = properties[i],
                attrValue = this.input.getAttribute("data-" + i.toLowerCase());

            if (typeof initial === "number") {
                this[i] = parseInt(attrValue);
            }
            else if (initial === false) { // Boolean options must be false by default anyway
                this[i] = attrValue !== null;
            }
            else if (initial instanceof Function) {
                this[i] = null;
            }
            else {
                this[i] = attrValue;
            }

            if (!this[i] && this[i] !== 0) {
                this[i] = (i in o) ? o[i] : initial;
            }
        }
    }

    // Helpers

    var slice = Array.prototype.slice;

    function $(expr, con) {
        return typeof expr === "string" ? (con || document).querySelector(expr) : expr || null;
    }

    function $$(expr, con) {
        return slice.call((con || document).querySelectorAll(expr));
    }

    $.create = function (tag, o) {
        var element = document.createElement(tag);

        for (var i in o) {
            var val = o[i];

            if (i === "inside") {
                $(val).appendChild(element);
            }
            else if (i === "around") {
                var ref = $(val);
                ref.parentNode.insertBefore(element, ref);
                element.appendChild(ref);
            } else if (i === "insertBefore") {
                var ref = $(val);
                ref.parentNode.insertBefore(element, ref);
            }
            else if (i in element) {
                element[i] = val;
            }
            else {
                element.setAttribute(i, val);
            }
        }

        return element;
    };

    $.bind = function (element, o) {
        if (element) {
            for (var event in o) {
                var callback = o[event];

                event.split(/\s+/).forEach(function (event) {
                    element.addEventListener(event, callback);
                });
            }
        }
    };

    $.fire = function (target, type, properties) {
        var evt = document.createEvent("HTMLEvents");

        evt.initEvent(type, true, true);

        for (var j in properties) {
            evt[j] = properties[j];
        }

        target.dispatchEvent(evt);
    };

    $.regExpEscape = function (s) {
        return s.replace(/[-\\^$*+?.()|[\]{}]/g, "\\$&");
    };

    $.createNewChoice = function (obj) {
        if (!obj || !obj.tagName) {
            throw { message: 'Invalid argument' };
        }

        var el = document.createElement(obj.tagName);
        obj.id && (el.id = obj.id);
        obj.className && (el.className = obj.className);
        obj.html && (el.innerHTML = obj.html);

        if (typeof obj.attributes !== 'undefined') {
            var attr = obj.attributes,
                prop;

            for (prop in attr) {
                if (attr.hasOwnProperty(prop)) {
                    el.setAttribute(prop, attr[prop]);
                }
            }
        }

        if (typeof obj.children !== 'undefined') {
            var child,
                i = 0;

            while (child = obj.children[i++]) {
                el.appendChild($.createNewChoice(child));
            }
        }

        return el;
    };

    // Initialization

    function init() {
        $$("input.autocomplete").forEach(function (input) {
            new Autocomplete(input);
        });
    }

    // Are we in a browser? Check for Document constructor
    if (typeof Document !== 'undefined') {
        // DOM already loaded?
        if (document.readyState !== "loading") {
            init();
        }
        else {
            // Wait for it
            document.addEventListener("DOMContentLoaded", init);
        }
    }

    //_.$ = $;
    //_.$$ = $$;

    // Make sure to export Autocomplete on self when in a browser
    if (typeof self !== 'undefined') {
        self.Autocomplete = _;
    }

    // Expose Autocomplete as a CJS module
    if (typeof exports === 'object') {
        module.exports = _;
    }

    return _;

}());